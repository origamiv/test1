<?php

namespace App\Console\Commands;

use App\Jobs\ArchiveEmailJob;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ArchiveOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:archive {--email=test@mail.ru}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Архивирование заказов и отправка архива на почту';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $archiveStartDate = Carbon::now()->subDays(90);
        $archiveEmail = $this->option('email');

        // Сейчас очереди через jobs, в боевом режиме все переведем на Redis
        ArchiveEmailJob::dispatch($archiveStartDate, $archiveEmail);
    }
}
