<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserService
{
    public function generateApiToken(User $user)
    {
        $token = Str::random(30);

        $user->update([
            'api_token' => $token
        ]);

        return $token;
    }

    public function authUser($email, $password)
    {
        $user = User::query()->where('email', $email)->first();
        if ($user && Hash::check($password, $user->password)){
            return $user;
        } else {
            return false;
        }
    }
}
