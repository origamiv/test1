<?php

namespace App\Services;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Http;

class OrderService
{
    public function getPaymentLink($order)
    {
        // Пример отправки запроса на платежный шлюз
        // Http::post('https://payment/get-payment-link', $order);

        return 'payment_link';
    }

    public function createArchive($email = null)
    {
        if (isset($email)){
            Artisan::call("order:archive --email=$email");
        } else {
            Artisan::call("order:archive");
        }
    }
}
