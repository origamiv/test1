<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Attachment;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class ArchiveMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $archive;

    protected $archiveStart;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($archive, $archiveStart)
    {
        $this->archive = $archive;
        $this->archiveStart = $archiveStart;
    }

    /**
     * Get the message envelope.
     *
     * @return \Illuminate\Mail\Mailables\Envelope
     */
    public function envelope()
    {
        return new Envelope(
            subject: 'Архив заказов за 3 месяца',
        );
    }

    /**
     * Get the message content definition.
     *
     * @return \Illuminate\Mail\Mailables\Content
     */
    public function content()
    {
        return new Content(
            view: 'mails.archive',
            with: [
                'archiveStart' => $this->archiveStart
            ]
        );
    }

    /**
     * Get the attachments for the message.
     *
     * @return array
     */
    public function attachments()
    {
        return [
            Attachment::fromPath($this->archive)
                ->as('orders.xlsx')
        ];
    }
}
