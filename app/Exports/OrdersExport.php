<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use App\Models\Order;

class OrdersExport implements FromCollection
{
    protected $start;

    public function __construct($start)
    {
        $this->start = $start;
    }

    public function collection()
    {
        return Order::query()
            ->where('created_at', '>=', $this->start)
            ->get();
    }
}
