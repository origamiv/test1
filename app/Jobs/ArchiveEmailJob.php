<?php

namespace App\Jobs;

use App\Exports\OrdersExport;
use App\Mail\ArchiveMail;
use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;

class ArchiveEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $archiveStartDate;
    protected $archiveEmail;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($archiveStartDate, $archiveEmail)
    {
        $this->archiveStartDate = $archiveStartDate;
        $this->archiveEmail = $archiveEmail;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $archive = Excel::download(new OrdersExport($this->archiveStartDate), 'orders.xlsx');
        $archivePath = $archive->getFile()->getPathname();

        Mail::to($this->archiveEmail)->send(new ArchiveMail($archivePath, $this->archiveStartDate));
    }
}
