<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\User;
use App\Services\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * @OA\Post(
     *     path="/api/user",
     *     summary="Создает пользователя",
     *     description="Создает пользователя",
     *     operationId="createUser",
     *     tags={"user"},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="name",
     *                     type="string",
     *                     description="Логин"
     *                 ),
     *                 @OA\Property(
     *                     property="email",
     *                     type="string",
     *                     description="Почта"
     *                 ),
     *                 @OA\Property(
     *                     property="password",
     *                     type="string",
     *                     description="Пароль"
     *                 ),
     *                 @OA\Property(
     *                     property="password_confirm",
     *                     type="string",
     *                     description="Подтверждение пароля"
     *                 ),
     *                 example={
     *                           "name": "test",
     *                           "email": "test@mail.ru",
     *                           "password": "12345678",
     *                           "password_confirm": "12345678"
     *                         }
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *         @OA\JsonContent(
     *             @OA\Examples(example="result", value={"success": true, "data": "user"}, summary="Пример ответа")
     *         )
     *     )
     * )
     */
    public function create(UserRequest $request): JsonResponse
    {
        $validated = $request->validated();
        $validated['password'] = Hash::make($validated['password']);
        $user = User::query()->create($validated);

        return response()->json(['success' => true, 'data' => $user]);
    }

    /**
     * @OA\Post(
     *     path="/api/user/token",
     *     summary="Создает API-токен",
     *     description="Создает API-токен для авторизации",
     *     operationId="generateUserToken",
     *     tags={"user"},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="email",
     *                     type="string",
     *                     description="Почта"
     *                 ),
     *                 @OA\Property(
     *                     property="password",
     *                     type="string",
     *                     description="Пароль"
     *                 ),
     *                 example={
     *                           "email": "test@mail.ru",
     *                           "password": "12345678",
     *                         }
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *         @OA\JsonContent(
     *             @OA\Examples(example="result", value={"success": true, "data": "token"}, summary="Пример ответа")
     *         )
     *     )
     * )
     */
    public function generateApiToken(Request $request): JsonResponse
    {
        $service = new UserService();

        $user = $service->authUser($request->email, $request->password);
        if ($user){
            $token = $service->generateApiToken($user);

            return response()->json(['success' => true, 'data' => $token]);
        } else {
            return response()->json(['success' => false, 'message' => 'Invalid login or password']);
        }
    }
}
