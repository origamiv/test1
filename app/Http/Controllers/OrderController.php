<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderRequest;
use App\Models\Order;
use App\Services\OrderService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * @OA\Info(
 *     title="API тестовое",
 *     version="1.0"
 * )
 * @OA\SecurityScheme(
 *   securityScheme="bearerAuth",
 *   in="header",
 *   name="Authorization",
 *   type="http",
 *   scheme="bearer",
 *   bearerFormat="JWT",
 * ),
 */

class OrderController extends Controller
{

    /**
     * @OA\Post(
     *     path="/api/order",
     *     summary="Создает заказ",
     *     description="Создает заказ и возвращает ссылку на оплату",
     *     operationId="createOrder",
     *     tags={"order"},
     *     security={
     *     {"bearerAuth": {}}
     *     },
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="number",
     *                     type="integer",
     *                     description="Номер заказа"
     *                 ),
     *                 @OA\Property(
     *                     property="email",
     *                     type="string",
     *                     description="E-mail"
     *                 ),
     *                 @OA\Property(
     *                     property="phone",
     *                     type="string",
     *                     description="Номер телефона"
     *                 ),
     *                 @OA\Property(
     *                     property="total",
     *                     type="integer",
     *                     description="Сумма заказа в рублях"
     *                 ),
     *                 example={
     *                           "number": 1,
     *                           "email": "test@mail.ru",
     *                           "phone": "+79991234567",
     *                           "total": 1000
     *                         }
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *         @OA\JsonContent(
     *             @OA\Examples(example="result", value={"success": true, "data": "payment_link"}, summary="Пример ответа")
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Error: Unauthorized",
     *     ),
     * )
     */
    public function create(OrderRequest $request): JsonResponse
    {
        $validated = $request->validated();
        $order = Order::query()->create($validated);

        $service = new OrderService();
        $paymentLink = $service->getPaymentLink($order);

        return response()->json(['success' => true, 'data' => $paymentLink]);
    }

    /**
     * @OA\Post(
     *     path="/api/order/archive",
     *     summary="Создает архив заказов",
     *     description="Создает архив заказов и отправляет на почту",
     *     operationId="createArchive",
     *     tags={"order"},
     *     security={
     *     {"bearerAuth": {}}
     *     },
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="email",
     *                     type="string",
     *                     description="E-mail"
     *                 ),
     *                 example={
     *                           "email": "test@mail.ru",
     *                         }
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *         @OA\JsonContent(
     *             @OA\Examples(example="result", value={"success": true}, summary="Пример ответа")
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Error: Unauthorized",
     *     ),
     * )
     */
    public function archive(Request $request): JsonResponse
    {
        $service = new OrderService();
        $service->createArchive($request->email);

        return response()->json(['success' => true]);
    }
}
